import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import EventEmitter from 'eventemitter3';
import Modal from 'react-modal';
import './App.css';

var my_ads = [
    {
        author: 'Саша Печкин',
        text: 'Абракадабра',
        date: '10.02.2016'
    },
    {
        author: 'Просто Вася',
        text: 'Считаю, что $ должен стоить 35 рублей!',
        date: '10.02.2016'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать без смс!',
        date: '21.06.2017'
    }
];

window.ee = new EventEmitter();

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

var Advert = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            author: React.PropTypes.string.isRequired,
            text: React.PropTypes.string.isRequired
        })
    },
    getInitialState: function() {
        return {
            isModalOpen: false,
            authorIsEmpty: true,
            textIsEmpty: true
        }
    },
    onFieldChange: function(fieldName, e) {
        if (e.target.value.trim().length > 0) {
            this.setState({[''+fieldName]:false})
        } else {
            this.setState({[''+fieldName]:true})
        }
    },
    onDeleteBtnClickHandler: function(e) {
        var item = this.props.data;
        window.ee.emit('Ads.delete', item);
    },
    openModal() {
        this.setState({ isModalOpen: true });
    },
    closeModal() {
        this.setState({ isModalOpen: false });
    },
    afterOpenModal() {
        var item = this.props.data;
        var author = ReactDOM.findDOMNode(this.refs.author);
        var text = ReactDOM.findDOMNode(this.refs.text);
        author.setRangeText(item.author);
        text.setRangeText(item.text);
        this.setState({textIsEmpty: false});
        this.setState({authorIsEmpty: false});
    },
    onEditBtnClickHandler: function(e) {
        e.preventDefault();
        var author = ReactDOM.findDOMNode(this.refs.author).value;
        var text = ReactDOM.findDOMNode(this.refs.text).value;

        var newItem = {
            author: author,
            text: text,
            date: getDate()
        };
        var item = this.props.data;

        window.ee.emit('Ads.edit', item, newItem);

        text = '';
        this.setState({textIsEmpty: true});
        this.closeModal();
    },
    render: function() {
        var author = this.props.data.author,
            text = this.props.data.text,
            date = this.props.data.date;

        var authorIsEmpty = this.state.authorIsEmpty,
            textIsEmpty = this.state.textIsEmpty;

        return (
            <div className='advert'>
                <p className='ads__author'>{author}:</p>
                <p className='ads__text'>{text}</p>
                <p className='ads__text'>{date}</p>
                <button
                    onClick={this.onDeleteBtnClickHandler}
                    ref='delete_button'
                >
                    Удалить
                </button>
                <div>
                    <button onClick={() => this.openModal()}>Редактировать</button>
                    <Modal
                        isOpen={this.state.isModalOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Example Modal"
                        ref = 'modalWindow'
                    >
                        <form className='add cf'>
                            <input
                                type='text'
                                className='add__author'
                                onChange={this.onFieldChange.bind(this, 'authorIsEmpty')}
                                placeholder='Ваше имя'
                                ref='author'
                            />
                            <textarea
                                className='add__text'
                                onChange={this.onFieldChange.bind(this, 'textIsEmpty')}
                                placeholder='Текст объявления'
                                ref='text'
                            ></textarea>
                            <button
                                className='add__btn'
                                onClick={this.onEditBtnClickHandler}
                                ref='add_button'
                                disabled={authorIsEmpty || textIsEmpty}
                            >
                                Сохранить объявление
                            </button>
                            <button onClick={this.closeModal}>Закрыть</button>
                        </form>
                    </Modal>
                </div>
            </div>
        )
    }
});

var Ads = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },
    getInitialState: function() {
        return {
            counter: 0
        }
    },
    render: function() {
        var data = this.props.data;
        var adsTemplate;

        if (data.length > 0) {
            adsTemplate = data.map(function(item, index) {
                return (
                    <div key={index}>
                        <Advert data={item} />
                    </div>
                )
            })
        } else {
            adsTemplate = <p>К сожалению объявлений нет</p>
        }

        return (
            <div className='ads'>
                {adsTemplate}
                <strong
                    className={'ads__count ' + (data.length > 0 ? '':'none') }>Всего объявлений: {data.length}</strong>
            </div>
        );
    }
});

function getDate() {
    var date = new Date();
    var month = date.getMonth();
    month = month < 10 ? '0' + month : month;
    var day = date.getDate();
    day = day < 10 ? '0' + day : day;
    var year = String(date.getFullYear());

    return day + "." + month + "." + year;
}

var Add = React.createClass({
    getInitialState: function() {
        return {
            authorIsEmpty: true,
            textIsEmpty: true
        };
    },
    onAddBtnClickHandler: function(e) {
        e.preventDefault();
        var author = ReactDOM.findDOMNode(this.refs.author).value;
        var text = ReactDOM.findDOMNode(this.refs.text).value;

        var item = [{
            author: author,
            text: text,
            date: getDate()
        }];

        window.ee.emit('Ads.add', item);

        text = '';
        this.setState({textIsEmpty: true});
        this.closeModal();
    },
    onFieldChange: function(fieldName, e) {
        if (e.target.value.trim().length > 0) {
            this.setState({[''+fieldName]:false})
        } else {
            this.setState({[''+fieldName]:true})
        }
    },
    openModal() {
        this.setState({ isModalOpen: true });
    },
    closeModal() {
        this.setState({ isModalOpen: false });
    },
    render: function() {
        var authorIsEmpty = this.state.authorIsEmpty,
            textIsEmpty = this.state.textIsEmpty;
        return (
            <div>
                <button onClick={() => this.openModal()}>Добавить</button>
                <Modal
                    isOpen={this.state.isModalOpen}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                    ref = 'modalWindow'
                >
                    <form className='add cf'>
                        <input
                            type='text'
                            className='add__author'
                            onChange={this.onFieldChange.bind(this, 'authorIsEmpty')}
                            placeholder='Ваше имя'
                            ref='author'
                        />
                        <textarea
                            className='add__text'
                            onChange={this.onFieldChange.bind(this, 'textIsEmpty')}
                            placeholder='Текст объявления'
                            ref='text'
                        ></textarea>
                        <button
                            className='add__btn'
                            onClick={this.onAddBtnClickHandler}
                            ref='add_button'
                            disabled={authorIsEmpty || textIsEmpty}
                        >
                            Опубликовать объявление
                        </button>
                        <button onClick={this.closeModal}>Закрыть</button>
                    </form>
                </Modal>
            </div>
        );
    }
});

var App = React.createClass({
    getInitialState: function() {
        return {
            ads: my_ads
        };
    },
    componentDidMount: function() {
        var self = this;
        window.ee.addListener('Ads.add', function(item) {
            var nextAds = item.concat(self.state.ads);
            self.setState({ads: nextAds});
        });
        window.ee.addListener('Ads.delete', function(item) {
            var index = self.state.ads.indexOf(item);
            self.state.ads.splice(index, 1);
            self.setState({ads: self.state.ads});
        });
        window.ee.addListener('Ads.edit', function(item, newItem) {
            var index = self.state.ads.indexOf(item);
            self.state.ads[index].author = newItem.author;
            self.state.ads[index].text = newItem.text;
            self.state.ads[index].date = newItem.date;
        });
    },
    componentWillUnmount: function() {
        window.ee.removeListener('Ads.add');
        window.ee.removeListener('Ads.delete');
        window.ee.removeListener('Ads.edit');
    },
    render: function() {
        return (
            <div className='app'>
                <h3>Объявления</h3>
                <Add />
                <Ads data={this.state.ads} />
            </div>
        );
    }
});

export default App;